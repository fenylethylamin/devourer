using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    WalkMode,
    PuzzleMode,
    DialogueMode,
}

public class GameManager : MonoBehaviour
{

    public GameObject _gunCrosshair;

    public GameObject _puzzleUIScreen;

    public GameObject _deathScreen;

    public static GameManager instance

    {
        get;
        private set;
    }

    [SerializeField] private GameObject _crosshairObject;
    [SerializeField] private GameState _gameState;
    [SerializeField] private GameObject _invitingText;
    [SerializeField] private Monologue _monologue;

    public int _currentPuzzleIndex = 0;

    public float waitAfterDying = 2f;

    public float waitAfterDeathScreen = 2f;

    [SerializeField] private Puzzle[] _puzzles;


    public Puzzle[] Puzzles { get => _puzzles; set => _puzzles = value; }

    public Monologue Monologue { get => _monologue; }



    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseUnpause();
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }

        else
        {
            Destroy(gameObject);
        }
}


    void Start()
    {
        SetCursor(false);
        SetGameState(GameState.WalkMode);
        InviteToPuzzle(false);
        _currentPuzzleIndex = 0;
        ActivateCurrentPuzzle();
        _gunCrosshair.SetActive(false);
    }

    public GameState GetGameState()
    {
        return _gameState;
    }

    public void SetGameState(GameState state)
    {
        _gameState = state;

    }

    public void SetCursor(bool show)
    {
        if (show)
        {
            Cursor.lockState = CursorLockMode.None;
            _crosshairObject.SetActive(false);
        }

        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            _crosshairObject.SetActive(true);
        }

    }

    public void ShowPuzzle()
    {
        SetCursor(true);
        SetGameState(GameState.PuzzleMode);
    }

    public void EscapePuzzle()
    {
        SetCursor(false);
        SetGameState(GameState.WalkMode);

    }

    public void InviteToPuzzle(bool active)

    {
        _invitingText.gameObject.SetActive(active);
    }

    public void AddLetterToCurrentPuzzle(char letter)

    {
        _puzzles[_currentPuzzleIndex].AddLetterToPuzzleGenerator(letter);
    }

    public void CompleteCurrentPuzzle()
    {
        _puzzles[_currentPuzzleIndex].CompletePuzzle();
        SetCursor(false);
        SetGameState(GameState.WalkMode);
        InviteToPuzzle(false);
        _currentPuzzleIndex++;
        ActivateCurrentPuzzle();
    }

    private void ActivateCurrentPuzzle()
    {
        for (int i = 0; i < _puzzles.Length; i++)
        {
            if (_currentPuzzleIndex == i)
            {
                _puzzles[i].gameObject.SetActive(true);
            }
            else
            {
                _puzzles[i].gameObject.SetActive(false);
            }
        }
    }

    public void PlayerDied()

    {
        StartCoroutine(PlayerDiedCo());
    }

    public IEnumerator PlayerDiedCo()

    {
        yield return new WaitForSeconds(waitAfterDying);

        _deathScreen.SetActive(true);


        yield return new WaitForSeconds(waitAfterDeathScreen);

        ES3AutoSaveMgr.Current.Load();

    }

    public void PauseUnpause()
    {
        if (UIController.instance.pauseScreen.activeInHierarchy)
        {
            UIController.instance.pauseScreen.SetActive(false);

            Cursor.lockState = CursorLockMode.Locked;

            Time.timeScale = 1f;

        }
        else
        {
            UIController.instance.pauseScreen.SetActive(true);

            Cursor.lockState = CursorLockMode.None;

            Time.timeScale = 0f;

        }
    }

    public void ShowGunCrosshair()

    {
        _gunCrosshair.SetActive(true);
    }
}





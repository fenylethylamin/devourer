using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JLiftAnimation : MonoBehaviour
{
    [SerializeField] public Animator _afterBossIsDeadAnimation;

    public BossHealthController _theBoss;

    public bool _isBossAlive;


public void DeadBoss()
    {
        if (_theBoss.currentHealth <= 0)

        {
            _isBossAlive = true;
        }

        else

        {
            _isBossAlive = false;
        }


    }

    public void LowerThePlatform()
    {
        if (!_isBossAlive)

        {
            _afterBossIsDeadAnimation.SetTrigger("died");

        }

    }

}



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour


{
    private bool chasing;
    public float chasingspeed = 5f;
    public float normalspeed = 1f;
    public Transform Player;
    public float distanceToChase = 10f, distanceToLose = 15f, distanceToStop = 2f;
    private Vector3 targetPoint, startPoint;
    public NavMeshAgent agent;
    public float keepChasingTime = 5f;
    private float chaseCounter;
    public bool wasShot;
    private Vector3 enemyPosition;
    public Vector3 walkPoint;
    public float walkFromCenter = 2f;
    private float timer;

    //shooting
    public GameObject bullet;
    public Transform firePoint1, firePoint2, firePoint3;
    public float fireRate, waitBetweenShots = 2f, timeToShoot = 1f;
    private float fireCount, shotWaitCounter, shootTimeCounter;
    //shooting


    void Start()
    {
        startPoint = transform.position;
        enemyPosition = transform.position;
        Patrolling();
        agent.speed = normalspeed;

        shootTimeCounter = timeToShoot;
        shotWaitCounter = waitBetweenShots;

    }


    void Update()
    {

        targetPoint = PlayerController.instance.transform.position;
        targetPoint.y = transform.position.y;

        if (!chasing)
        {
            if (Vector3.Distance(transform.position, targetPoint) < distanceToChase)
            {
                chasing = true;
                shootTimeCounter = timeToShoot;
                shotWaitCounter = waitBetweenShots;

            }

            if (chaseCounter > 0)
            {
                chaseCounter -= Time.deltaTime;

                if (chaseCounter <= 0)
                {
                    agent.destination = startPoint;
                }

                if (timer > 2f)
                {
                    timer = 0;
                    Patrolling();
                }
            }
        }

        else
        {
            agent.speed = chasingspeed;
            transform.LookAt(Player);

            if (Vector3.Distance(transform.position, targetPoint) > distanceToStop)
            {
                agent.destination = targetPoint;
            }
            else
            {
                agent.destination = walkPoint;
                timer += Time.deltaTime;
                if (timer > 2f)
                {
                    timer = 0;
                    Patrolling();
                }
            }

            if (Vector3.Distance(transform.position, targetPoint) > distanceToLose)
            {
                if (!wasShot)
                {
                    chasing = false;
                    chaseCounter = keepChasingTime;

                }
            }

            else
            {
                wasShot = false;
            }

            if (shotWaitCounter > 0)

            {
                shotWaitCounter -= Time.deltaTime;

                if (shotWaitCounter <= 0)
                {
                    shootTimeCounter = timeToShoot;
                }
            }
            else

            {

                if (PlayerController.instance.gameObject.activeInHierarchy)

                {
                    shootTimeCounter -= Time.deltaTime;

                    if (shootTimeCounter > 0)
                    {
                        fireCount -= Time.deltaTime;
                        if (fireCount <= 0)
                        {
                            fireCount = fireRate;

                            //limiting the fire angle
                            firePoint1.LookAt(PlayerController.instance.transform.position + new Vector3(0f, 1.2f, 0f));
                            firePoint2.LookAt(PlayerController.instance.transform.position + new Vector3(0f, 1.2f, 0f));
                            firePoint3.LookAt(PlayerController.instance.transform.position + new Vector3(0f, 1.2f, 0f));

                            //check the angle to the player
                            Vector3 targetDir = PlayerController.instance.transform.position - transform.position;
                            float angle = Vector3.SignedAngle(targetDir, transform.forward, Vector3.up);

                            if (Mathf.Abs(angle) < 30f)
                            {

                                Instantiate(bullet, firePoint1.position, firePoint1.rotation);
                                Instantiate(bullet, firePoint2.position, firePoint2.rotation);
                                Instantiate(bullet, firePoint3.position, firePoint3.rotation);

                            }
                            else

                            {
                                shotWaitCounter = waitBetweenShots;
                            }
                        }

                        agent.destination = transform.position;


                    }
                    else
                    {
                        shotWaitCounter = waitBetweenShots;
                    }



                }
            }
        }
    }

    private void Patrolling()

    {

        float randomZ = Random.Range(-walkFromCenter, walkFromCenter) + enemyPosition.z;
        float randomX = Random.Range(-walkFromCenter, walkFromCenter) + +enemyPosition.x;

        walkPoint = new Vector3(randomX, transform.position.y, randomZ);

    }

    public void GetShot()
    {
        wasShot = true;

        chasing = true;

    }
}




using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthController : MonoBehaviour
{

	public int currentHealth;
	public EmojiAIController theEC;

	public void DamageEnemy(int damageAmount)
	{
		currentHealth -= damageAmount;

		if (theEC != null)
		{
			theEC.GetShot();
		}

		if (currentHealth <= 0)
		{
			Die();
		}
	}

	private void Die()
	{
		var s = GetComponent<SpawnObjectOnDeath>();
		if (s != null) s.SpawnObject();


	}

}